from django.db import models

# Create your models here.

class Employe(models.Model):
    nom = models.CharField(max_length=100)
    adresse = models.TextField()
    age = models.IntegerField()
    profession = models.CharField(max_length=100)

    def __str__(self):
        return self.nom

class Entreprise(models.Model):
    nom = models.CharField(max_length=100)
    adresse = models.TextField()
    description = models.TextField()

    def __str__(self):
        return self.no
