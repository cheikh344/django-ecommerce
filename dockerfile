# Utilisez une image Python -
FROM python:3.9

# Créez un répertoire de travail
WORKDIR /app

# Copiez les fichiers de votre application Django dans le conteneur
COPY . .
COPY core core
COPY static_in_env static_in_env
COPY templates templates
COPY djecommerce  djecommerce
COPY manage.py .
COPY .env .

# Installez les dépendances de l'application
RUN pip install --upgrade pip
RUN pip install virtualenv
RUN virtualenv env

RUN pip install -r requirements.txt
RUN apt-get update && apt-get install -y postgresql-client
RUN pip install psycopg2-binary

RUN python manage.py makemigrations
RUN python manage.py migrate
RUN python manage.py collectstatic --noinput


# Exposez le port sur lequel l'application écoute (par exemple, 8000)
EXPOSE 8000

# Commande pour exécuter l'application au démarrage du conteneur
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
